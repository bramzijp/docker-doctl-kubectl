docker build -t doctl-kubectl .
docker stop doctl-kubectl
docker rm doctl-kubectl

docker run --name doctl-kubectl -e DIGITALOCEAN_ACCESS_TOKEN="secret" -e DIGITALOCEAN_CLUSTER_NAME="secret" -it doctl-kubectl sh