# Why:
As you use Docker images in Gitlab CI/CD, I created this image to manage your Kubernetes cluster on Digital Ocean from within a Docker container (manually) and with ease.

# Usage:
- Use the following image `registry.gitlab.com/bramzijp/docker-doctl-kubectl/doctl-kubectl:1.22.0` in your Gitlab CI/CD config.
- Set the following environment variables inside of your Gitlab CI/CD settings
  - `DIGITALOCEAN_ACCESS_TOKEN`
  - `DIGITALOCEAN_CLUSTER_NAME`

# Example Gitlab CI/CD configuration file:
```
stages:
  - release

release:
  stage: release
  image: registry.gitlab.com/bramzijp/docker-doctl-kubectl/doctl-kubectl:1.22.0
  only:
    - master
  script:
    - kubectl get pods
```

# Use cases:
- Use as image in Gitlab CI/CD or other CI/CD solutions that require docker images.
- Update the env variables in the test.sh file and use for doctl/kubectl/ssh server management on your machine.