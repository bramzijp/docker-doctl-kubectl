FROM alpine:3.10

LABEL version="1"
LABEL description="Docker image for deploying to Digital Ocean manually"
LABEL maintainer="Bram Zijp <info@bramzijp.nl>"


ENV DOCTL_VERSION=1.22.0
ENV KUBECTL_VERSION=1.18.10
ENV ISTIO_VERSION=1.4.3

ENV PATH="/root/istio-${ISTIO_VERSION}/bin:${PATH}"

WORKDIR /root

RUN apk add ca-certificates curl git


RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2


RUN wget -c https://github.com/digitalocean/doctl/releases/download/v${DOCTL_VERSION}/doctl-${DOCTL_VERSION}-linux-amd64.tar.gz -O - | tar -xz; \
  chmod +x ./doctl; \
  ln ./doctl /usr/local/bin/doctl;


RUN wget -c https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl; \
  chmod +x ./kubectl; \
  ln ./kubectl /usr/local/bin/kubectl;

RUN curl -L https://istio.io/downloadIstio | ISTIO_VERSION=${ISTIO_VERSION} sh -;


COPY entrypoint.sh entrypoint.sh

RUN chmod +x entrypoint.sh


ENTRYPOINT ["./entrypoint.sh"]

CMD tail -f /dev/null