#!/bin/sh

doctl auth init;

doctl kubernetes cluster kubeconfig save $DIGITALOCEAN_CLUSTER_NAME;

exec "$@"